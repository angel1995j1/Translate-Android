package com.example.winapp.feedback.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.winapp.feedback.R;
import com.example.winapp.feedback.fragment.ProductsFragment;
import com.example.winapp.feedback.models.Category;
import com.example.winapp.feedback.utils.SystemPref;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoyHolder> {
    List<Category> categoryList;
    Context context;

    public CategoryAdapter(List<Category> categoryList, Context context) {
        this.categoryList = categoryList;
        this.context = context;
    }

    @NonNull
    @Override
    public CategoyHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.categoty_list,viewGroup,false);
        return new CategoyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoyHolder categoyHolder, int i) {
        Category category = categoryList.get(i);
        categoyHolder.cat_image.setImageResource(category.getImage());
        categoyHolder.cat_name.setText(category.getName());
        categoyHolder.categ_layout.setOnClickListener(view->{
            new SystemPref().openFragment(ProductsFragment.newInstance(category.getId()),context);
        });
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    protected class CategoyHolder extends RecyclerView.ViewHolder {
        TextView cat_name;
        ImageView cat_image;
        CardView categ_layout;
        public CategoyHolder(@NonNull View view) {
            super(view);
            cat_image = view.findViewById(R.id.cat_image);
            cat_name = view.findViewById(R.id.cat_name);
            categ_layout = view.findViewById(R.id.categ_layout);
        }
    }
}
