package com.example.winapp.feedback;

import android.content.Intent;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.example.winapp.feedback.utils.SharePref;

public class SplashActivity extends AppCompatActivity {
    private final int TIME_OUT = 2000;
    SharePref pref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        pref = new SharePref(this);
        openMainActivity();
    }
    public  void openMainActivity(){
        new Handler().postDelayed(()->{
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
            finish();
        },TIME_OUT);
    }
}
