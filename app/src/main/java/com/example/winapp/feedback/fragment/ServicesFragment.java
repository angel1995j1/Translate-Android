package com.example.winapp.feedback.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.winapp.feedback.R;
import com.example.winapp.feedback.mail.GMailSender;
import com.example.winapp.feedback.models.UserModel;
import com.example.winapp.feedback.utils.Constants;
import com.example.winapp.feedback.utils.KeyboardUtils;
import com.example.winapp.feedback.utils.SharePref;

import java.util.ArrayList;
import java.util.List;

public class ServicesFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private Spinner service_lists;
    Button submit;
    TextView feed_back;
    SharePref  pref;
    UserModel userModel;
    public ServicesFragment() {
        // Required empty public constructor
    }


    public static ServicesFragment newInstance(String param1, String param2) {
        ServicesFragment fragment = new ServicesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_services, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pref = new SharePref(getContext());
        userModel = pref.getObject();
        setXML(view);
    }
    public void setXML(View view){
        service_lists = view.findViewById(R.id.service_lists);
        submit = view.findViewById(R.id.submit);
        feed_back = view.findViewById(R.id.feed_back);
        service_lists.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Averías Tractor");
        categories.add("Mantenimiento");
        categories.add("Sugerencias");
        categories.add("Otros");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        service_lists.setAdapter(dataAdapter);
        submit.setOnClickListener(v ->{
            KeyboardUtils.hideKeyboard(Constants.getActivity());
            new SendMail().execute();
            Toast.makeText(getContext(), "Feed Back Submit Successfully", Toast.LENGTH_LONG).show();
            feed_back.setText("");
        });
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        // Showing selected spinner item
       // Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    class SendMail extends AsyncTask{
        @Override
        protected Object doInBackground(Object[] objects) {
            try {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                        .permitAll().build();
                StrictMode.setThreadPolicy(policy);
                GMailSender sender = new GMailSender("abdullahmasood2x@gmail.com", "");
                sender.sendMail("Submit from Landini",
                        "UserName: " + userModel.getFirstName()+" "+userModel.getLastName()+"\n"+
                                "Address: "+ userModel.getAddress()+"\n"+
                                "Telephone: "+userModel.getTelephone()+"\n"+
                                "Service Name:"+service_lists.getSelectedItem().toString()+"\n"
                                +"FeedBack: "+feed_back.getText().toString(),
                        "abdullahmasood2x@gmail.com",
                        "abdullahmasood2x@gmail.com");
            } catch (Exception e) {
                Log.e("SendMail", e.getMessage(), e);
            }
            return null;
        }
    }
}
