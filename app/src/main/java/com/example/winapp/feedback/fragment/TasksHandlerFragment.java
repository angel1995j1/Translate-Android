package com.example.winapp.feedback.fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.winapp.feedback.R;
import com.example.winapp.feedback.adapters.NotificationAdapter;
import com.example.winapp.feedback.models.NotificationsModel;
import com.example.winapp.feedback.utils.RecyleViewHandler;
import com.example.winapp.feedback.utils.VerticalSpaceItemDecoration;

import java.util.ArrayList;
import java.util.List;

public class TasksHandlerFragment extends Fragment {

    RecyclerView tasks_list;
    public TasksHandlerFragment() {
        // Required empty public constructor
    }


    public static TasksHandlerFragment newInstance(String param1, String param2) {
        TasksHandlerFragment fragment = new TasksHandlerFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tasks_handler, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tasks_list = view.findViewById(R.id.tasks_list);
        setupRecyelView();
        addData();
    }



    public void setupRecyelView(){
        RecyleViewHandler recyclerViewHandler = new RecyleViewHandler();
        tasks_list = recyclerViewHandler.
                settingRecyleView(tasks_list, LinearLayoutManager.VERTICAL,getContext());
        tasks_list.addItemDecoration(new VerticalSpaceItemDecoration(20));
        tasks_list.setItemAnimator(new DefaultItemAnimator());
    }
    public void addData(){
        List<NotificationsModel> notificationsModelList = new ArrayList<>();
        //  One
        NotificationsModel notificationsModel = new NotificationsModel();
        notificationsModel.setTitle("Clorosis");
        notificationsModel.setType("Enfermedad");
        notificationsModel.setDate("01/04/2019");
        notificationsModel.setAuthor("Javier Martín");
        notificationsModel.setAddress("41.608287,-4.101669");
        notificationsModel.setComments("Posibles deficiencias de nutrientes. Se extiende hacia la zona sur.");
        notificationsModel.setImage(R.drawable.one);
        //  Two
        NotificationsModel notificationsModelTwo = new NotificationsModel();
        notificationsModelTwo.setTitle("Araña Roja");
        notificationsModelTwo.setType("Plaga");
        notificationsModelTwo.setDate("30/03/2019 ");
        notificationsModelTwo.setAuthor("Javier Martín");
        notificationsModelTwo.setAddress("41.603847,-4.095189");
        notificationsModelTwo.setComments("Parece araña roja o ácaros, echad un ojo.");
        notificationsModelTwo.setImage(R.drawable.one);


        notificationsModelList.add(notificationsModel);
        notificationsModelList.add(notificationsModelTwo);

        NotificationAdapter notificationAdapter = new NotificationAdapter(notificationsModelList,getContext());
        tasks_list.setAdapter(notificationAdapter);
    }
}
