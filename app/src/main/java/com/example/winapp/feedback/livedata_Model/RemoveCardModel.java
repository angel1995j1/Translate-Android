package com.example.winapp.feedback.livedata_Model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.winapp.feedback.models.Products;

public class RemoveCardModel extends ViewModel {

    private MutableLiveData<Products> quantity;

    public MutableLiveData<Products> getProductRemove(){
        if(quantity==null){
            quantity  = new MutableLiveData<>();
        }
        return  quantity;
    }
}
