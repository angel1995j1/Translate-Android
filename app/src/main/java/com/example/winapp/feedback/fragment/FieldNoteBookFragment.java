package com.example.winapp.feedback.fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.winapp.feedback.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class FieldNoteBookFragment extends Fragment {

    Calendar myCalendar;
    EditText start_date,end_date;
    Spinner select_labour,select_fields;
    public FieldNoteBookFragment() {
        // Required empty public constructor
    }


    public static FieldNoteBookFragment newInstance(String param1, String param2) {
        FieldNoteBookFragment fragment = new FieldNoteBookFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_field_note_book, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        myCalendar = Calendar.getInstance();
        setXML(view);
    }
    public void setXML(View view){
        start_date = view.findViewById(R.id.start_date);
        select_labour = view.findViewById(R.id.select_labour);
        select_fields = view.findViewById(R.id.select_fields);
        end_date = view.findViewById(R.id.end_date);
        start_date.setOnClickListener(v->{
            DatePickerDialog datePickerDialog = new  DatePickerDialog(getContext(), startDate, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            //       datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis()+(1000*60*60*24*7)); //After 7 Days from Now
                datePickerDialog.show();
        });
        end_date.setOnClickListener(v1->{
            DatePickerDialog datePickerDialog = new  DatePickerDialog(getContext(), endDate, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            //       datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis()+(1000*60*60*24*7)); //After 7 Days from Now
               datePickerDialog.show();
        });
        addLabours();
        addFields();

    }
    DatePickerDialog.OnDateSetListener startDate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            start_date.setText(String.valueOf(dayOfMonth)+"-"+String.valueOf(monthOfYear)+"-"+String.valueOf(year));
        }
    };
    DatePickerDialog.OnDateSetListener endDate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            end_date.setText(String.valueOf(dayOfMonth)+"-"+String.valueOf(monthOfYear)+"-"+String.valueOf(year));

        }
    };
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
    public void addLabours(){
        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Otros");
        categories.add("Abonados");
        categories.add("Fertilizantes");
        categories.add("Fitosanitarios");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        select_labour.setAdapter(dataAdapter);
    }
    public void addFields(){
        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Otros");
        categories.add("PDC 1");
        categories.add("PDC 2");
        categories.add("PSTM 1");
        categories.add("PSTM 2");


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        select_fields.setAdapter(dataAdapter);
    }
}
