package com.example.winapp.feedback.livedata_Model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.winapp.feedback.models.Products;

public class DataModel extends ViewModel {

    private MutableLiveData<Products> quantity;

    public MutableLiveData<Products> getCurrentName(){
        if(quantity==null){
            quantity  = new MutableLiveData<>();
        }
        return  quantity;
    }



}
