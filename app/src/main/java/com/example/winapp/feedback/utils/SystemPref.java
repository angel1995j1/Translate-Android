package com.example.winapp.feedback.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.winapp.feedback.R;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SystemPref {
    public String convertToString(Object object){
        return String.valueOf(object);
    }
    public String getWeekDay(){
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        return sdf.format(d);
    }
    public void openFragment(Fragment fragment, Context activity){
        ((FragmentActivity)activity).getSupportFragmentManager().
                beginTransaction().replace(R.id.frame,fragment).addToBackStack(null).commit();
    }
    public void openFragmentNoStack(Fragment fragment, Context activity){
        ((FragmentActivity)activity).getSupportFragmentManager().
                beginTransaction().replace(R.id.frame,fragment).commit();
    }
    public static View getLayout(ViewGroup viewGroup,int layout){
        return   LayoutInflater.from(viewGroup.getContext()).inflate(layout,
                viewGroup,false);

    }
    public static void openMap(Context context,String latitude, String longitude){
        String uri = "http://maps.google.com/maps?daddr=" +latitude + "," + longitude;
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        context.startActivity(intent);
    }
    public static void makeCall(Context context,String number){
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:"+number));
        context.startActivity(callIntent);
    }
    public static void openMail(Context context,String email){
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto",email, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        context.startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }
}
