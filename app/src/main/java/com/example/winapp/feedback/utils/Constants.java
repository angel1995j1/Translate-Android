package com.example.winapp.feedback.utils;

import android.app.Activity;

public class Constants {
    public static final String W_BASE_URL="http://api.openweathermap.org/data/2.5/weather?";
    public static final String W_UNIT="&units=metric&APPID=cf5f3ad4637916a3db22b0493d712f4c";
    public static final String WEATHER_IMAGE="http://openweathermap.org/img/w/";
    public static final String WEATHER_UVINDEX="http://api.openweathermap.org/data/2.5/uvi?";
    public static final String SMARTURAL_URL ="https://smartrural.net";
    public static final String LANDINI_URL ="https://www.landini.it/es/";
    public static final int PRODUCT_LIST =0;
    public static final int CART_LIST =1;
    public static final String WEB_TYPE = "webtypes";

    public static Activity activityy;

    //"lat=31.520370&lon=74.358749
    public static String LoadUrl(String BASE_URL,String lati,String lon){
        return BASE_URL+"lat="+lati+"&lon="+lon+W_UNIT;
    }
    public static String LoadImageUrl(String name){
        return WEATHER_IMAGE+name+".png";
    }

    public static Activity getActivity() {
        return activityy;
    }

    public static void setActivity(Activity activity) {
        activityy = activity;
    }
}
