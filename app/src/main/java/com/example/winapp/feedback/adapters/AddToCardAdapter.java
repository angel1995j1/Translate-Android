package com.example.winapp.feedback.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.winapp.feedback.R;
import com.example.winapp.feedback.models.Products;

import java.util.List;

public class AddToCardAdapter extends RecyclerView.Adapter<AddToCardAdapter.AddToCardHolder> {
    List<Products> productsList;
    Context context;

    public AddToCardAdapter(List<Products> productsList, Context context) {
        this.productsList = productsList;
        this.context = context;
    }

    @NonNull
    @Override
    public AddToCardHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull AddToCardHolder addToCardHolder, int i) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    protected class AddToCardHolder extends RecyclerView.ViewHolder {
        TextView productName,productNo;
        ImageView productImage,add_tocard;
        public AddToCardHolder(@NonNull View view) {
            super(view);
            productName = view.findViewById(R.id.product_name);
            productNo = view.findViewById(R.id.product_no);
            productImage = view.findViewById(R.id.product_image);
            add_tocard = view.findViewById(R.id.add_tocard);
        }
    }
}
