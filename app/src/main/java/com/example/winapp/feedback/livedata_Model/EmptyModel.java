package com.example.winapp.feedback.livedata_Model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.winapp.feedback.models.Products;


public class EmptyModel  extends ViewModel {

    private MutableLiveData<Products> quantity;

    public MutableLiveData<Products> removeValue(){
        if(quantity==null){
            quantity  = new MutableLiveData<>();
        }
        return  quantity;
    }
}
