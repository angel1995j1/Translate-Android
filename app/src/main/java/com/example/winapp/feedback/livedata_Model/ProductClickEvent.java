package com.example.winapp.feedback.livedata_Model;

import com.example.winapp.feedback.models.Products;

public interface ProductClickEvent  {
    void addToCard(Products products);
    void removeCard(Products products);
    void deleteItem(int postion);
}
