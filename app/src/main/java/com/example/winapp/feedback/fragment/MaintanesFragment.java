package com.example.winapp.feedback.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.winapp.feedback.R;
import com.example.winapp.feedback.utils.Constants;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class MaintanesFragment extends Fragment implements View.OnClickListener {

    private static final int MY_CAMERA_PERMISSION_CODE = 2000;
    private static final int MY_GALLREY_PERMISSION_CODE = 3000;
    ImageView upload_image;
    Spinner select_disease;
    private final int PERMISSION_CODE = 1000;
    String permission[] = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};
    private File output = null;
    private Uri file;
    Spinner select_maintance;
    public MaintanesFragment() {
    }

    public static MaintanesFragment newInstance(String param1, String param2) {
        MaintanesFragment fragment = new MaintanesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_maintanes, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setXML(view);
    }

    public void setXML(View view){
        ActivityCompat.requestPermissions(Constants.getActivity(),
                permission,
                PERMISSION_CODE);
        select_maintance = view.findViewById(R.id.select_maintance);
        upload_image = view.findViewById(R.id.upload_image);
        upload_image.setOnClickListener(this);
        addDieaselists();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
    public void addDieaselists(){

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Otros");
        categories.add("Rotura de riego");
        categories.add("Subir espalderas");
        categories.add("Alambre roto");
        categories.add("Cepa Dañada");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        select_maintance.setAdapter(dataAdapter);
    }
    public void selectDialogOption() {

        Dialog dialog = new Dialog(Constants.getActivity());
        dialog.setContentView(R.layout.camera_list);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        Button openCamera = dialog.findViewById(R.id.open_camera);
        Button openGallery = dialog.findViewById(R.id.open_gallery);
        ImageView close = dialog.findViewById(R.id.close);
        close.setOnClickListener(v -> {
            dialog.dismiss();
        });
        openCamera.setOnClickListener(v1 -> {
            dialog.dismiss();
            if (Constants.getActivity().checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, PERMISSION_CODE);
            } else {
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                file = Uri.fromFile(getOutputMediaFile());
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, file);
                startActivityForResult(cameraIntent, MY_CAMERA_PERMISSION_CODE);
            }
        });
        openGallery.setOnClickListener(v3 -> {
            dialog.dismiss();
            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(pickPhoto, MY_GALLREY_PERMISSION_CODE);//zero can be replaced with any action code
        });


        dialog.show();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.upload_image:
                selectDialogOption();
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (requestCode == PERMISSION_CODE) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED ||
                    grantResults[1] != PackageManager.PERMISSION_GRANTED ||
                    grantResults[2] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Permission required", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                //  Bitmap bitmap  = (Bitmap) data.getExtras().get("data");
                upload_image.setImageURI(file);
                // storeCameraPhotoInSDCard(bitmap,String.valueOf(new Date().getTime()));
            }else{
                upload_image.setImageDrawable(getContext().getDrawable(R.drawable.camera));
            }
        }
        if(requestCode==MY_GALLREY_PERMISSION_CODE){
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getData();
                upload_image.setImageURI(uri);
            }else{
                upload_image.setImageDrawable(getContext().getDrawable(R.drawable.camera));

            }
        }
    }

    private static File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "CameraDemo");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");
    }
}
