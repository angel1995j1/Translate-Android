package com.example.winapp.feedback.livedata_Model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MyViewHolder extends ViewModel {
    private MutableLiveData<String> currentName;

    public MutableLiveData<String> getCurrentName(){
        if(currentName==null){
            currentName = new MutableLiveData<>();
        }
        return currentName;
    }
}
