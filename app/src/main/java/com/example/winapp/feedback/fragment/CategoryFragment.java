package com.example.winapp.feedback.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.winapp.feedback.R;
import com.example.winapp.feedback.adapters.CategoryAdapter;
import com.example.winapp.feedback.models.Category;

import java.util.ArrayList;
import java.util.List;

public class CategoryFragment extends Fragment {

    private RecyclerView recyclerView;
    private ProgressBar progress_circular;
    private List<Category> categoryList;
    public CategoryFragment() {
        // Required empty public constructor
    }

    public static CategoryFragment newInstance(String param1, String param2) {
        CategoryFragment fragment = new CategoryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_category, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setXML(view);
    }
    public void setXML(View view){
        recyclerView = view.findViewById(R.id.cate_list);
        progress_circular = view.findViewById(R.id.progress_circular);
        setList();
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }
    @Override
    public void onDetach() {
        super.onDetach();
    }
    public void setList(){
        categoryList = new ArrayList<>();
        categoryList.add(new Category(1,"Neumaticos",R.drawable.cat_1));
        categoryList.add(new Category(2,"Tractores",R.drawable.cate_3));
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
       /* ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getActivity(),
                (int)getContext().getResources().getDimension(R.dimen.item_offset));
        recyclerView.addItemDecoration(itemDecoration);*/
        CategoryAdapter adapter = new CategoryAdapter(categoryList,getContext());
        recyclerView.setAdapter(adapter);
    }
}
