package com.example.winapp.feedback.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.winapp.feedback.R;
import com.example.winapp.feedback.models.LocationModel;
import com.example.winapp.feedback.utils.SystemPref;

import java.util.ArrayList;
import java.util.List;

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.LocationViewHolder>
implements Filterable {
    List<LocationModel> locationModelsList;
    List<LocationModel> filderLocationList;
    Context context;

    public LocationAdapter(List<LocationModel> locationModelsList, Context context) {
        this.locationModelsList = locationModelsList;
        this.context = context;
        this.filderLocationList = locationModelsList;
    }

    @NonNull
    @Override
    public LocationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate
                (R.layout.location_list,viewGroup,false);
        return new LocationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LocationViewHolder holder, int i) {
        LocationModel locationModel = locationModelsList.get(i);
        holder.location_name.setText(locationModel.getName());
        holder.location_address.setText(locationModel.getAddress());
        holder.location_city.setText(locationModel.getCity());
        holder.location_coordinates.setText(locationModel.getLocation());
        holder.location_phone.setText(locationModel.getPhone());
        holder.location_province.setText(locationModel.getProvince());
        holder.location_email.setText(locationModel.getEmail());
        holder.main_layout.setOnClickListener(v->{
            Toast.makeText(context,locationModel.getLocation(),Toast.LENGTH_LONG).show();
            String[] tokenizer =locationModel.getLocation().split(",");
            String latitude = tokenizer[0];
            String longitude = tokenizer[1];
            SystemPref.openMap(context,latitude,longitude);
        });
        holder.location_phone.setOnClickListener(v1->{
            SystemPref.makeCall(context,locationModel.getPhone());
        });
        holder.location_email.setOnClickListener(v3->{
            SystemPref.openMail(context,locationModel.getEmail());
        });
    }

    @Override
    public int getItemCount() {
        return locationModelsList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
              String charString = constraint.toString();
              if(charString.isEmpty()){
                  filderLocationList = locationModelsList;
              }else{
                  List<LocationModel>  filterData = new ArrayList<>();
                  for(LocationModel location: locationModelsList){
                      if(location.getProvince().toLowerCase().contains(charString.toLowerCase())){
                          filterData.add(location);

                      }
                  }
                  filderLocationList = filterData;
              }
                FilterResults filer = new FilterResults();
                filer.values = filderLocationList;
                return filer;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filderLocationList = (ArrayList<LocationModel>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    protected class LocationViewHolder extends RecyclerView.ViewHolder {
        TextView location_name,location_address,location_email,location_phone,
                location_city,location_province,location_coordinates;
        LinearLayout main_layout;
        public LocationViewHolder(@NonNull View view) {
            super(view);
            location_name = view.findViewById(R.id.location_name);
            location_address = view.findViewById(R.id.location_address);
            location_email = view.findViewById(R.id.location_email);
            location_phone = view.findViewById(R.id.location_phone);
            location_city = view.findViewById(R.id.location_city);
            location_province = view.findViewById(R.id.location_province);
            location_coordinates = view.findViewById(R.id.location_coordinates);
            main_layout = view.findViewById(R.id.main_layout);
        }
    }

}
