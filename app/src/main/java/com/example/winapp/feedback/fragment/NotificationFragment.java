package com.example.winapp.feedback.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.winapp.feedback.R;
import com.example.winapp.feedback.adapters.NotificationAdapter;
import com.example.winapp.feedback.models.NotificationsModel;
import com.example.winapp.feedback.utils.RecyleViewHandler;
import com.example.winapp.feedback.utils.VerticalSpaceItemDecoration;

import java.util.ArrayList;
import java.util.List;


public class NotificationFragment extends Fragment {

    RecyclerView notification_list;
    public NotificationFragment() {
        // Required empty public constructor
    }


    public static NotificationFragment newInstance(String param1, String param2) {
        NotificationFragment fragment = new NotificationFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notification, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        notification_list  = view.findViewById(R.id.notification_list);
        setupRecyelView();
        addData();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void setupRecyelView(){
        RecyleViewHandler recyclerViewHandler = new RecyleViewHandler();
        notification_list = recyclerViewHandler.
                settingRecyleView(notification_list, LinearLayoutManager.VERTICAL,getContext());
        notification_list.addItemDecoration(new VerticalSpaceItemDecoration(20));
        notification_list.setItemAnimator(new DefaultItemAnimator());
    }
    public void addData(){
        List<NotificationsModel>   notificationsModelList = new ArrayList<>();
        //  One
        NotificationsModel notificationsModel = new NotificationsModel();
        notificationsModel.setTitle("Clorosis");
        notificationsModel.setType("Enfermedad");
        notificationsModel.setDate("01/04/2019 - 8:21");
        notificationsModel.setAuthor("Javier Martín");
        notificationsModel.setAddress("41.608287,-4.101669");
        notificationsModel.setComments("Posibles deficiencias de nutrientes. Se extiende hacia la zona sur.");
        notificationsModel.setImage(R.drawable.one);
        //  Two
        NotificationsModel notificationsModelTwo = new NotificationsModel();
        notificationsModelTwo.setTitle("Araña Roja");
        notificationsModelTwo.setType("Plaga");
        notificationsModelTwo.setDate("30/03/2019 -  9:41");
        notificationsModelTwo.setAuthor("Javier Martín");
        notificationsModelTwo.setAddress("41.603847,-4.095189");
        notificationsModelTwo.setComments("Parece araña roja o ácaros, echad un ojo.");
        notificationsModelTwo.setImage(R.drawable.two);

        //  Three
        NotificationsModel notificationsModelThree = new NotificationsModel();
        notificationsModelThree.setTitle("Riego roto");
        notificationsModelThree.setType("Mantenimiento");
        notificationsModelThree.setDate("26/03/2019 - 15:53");
        notificationsModelThree.setAuthor("Alberto Rodríguez");
        notificationsModelThree.setAddress("41.589601,-4.085616");
        notificationsModelThree.setComments("Sale a chorro y se encharca todo");
        notificationsModelThree.setImage(R.drawable.three);

        //  Four
        NotificationsModel notificationsModelFour = new NotificationsModel();
        notificationsModelFour.setTitle("Subir Espalderas");
        notificationsModelFour.setType("Mantenimiento");
        notificationsModelFour.setDate("26/03/2019 - 15:53");
        notificationsModelFour.setAuthor("Javier Martín");
        notificationsModelFour.setAddress("41.603847,-4.095189");
        notificationsModelFour.setComments("Retensar el alambre");
        notificationsModelFour.setImage(R.drawable.four);

        notificationsModelList.add(notificationsModel);
        notificationsModelList.add(notificationsModelTwo);
        notificationsModelList.add(notificationsModelThree);
        notificationsModelList.add(notificationsModelFour);
        NotificationAdapter notificationAdapter = new NotificationAdapter(notificationsModelList,getContext());
        notification_list.setAdapter(notificationAdapter);
    }
}
