package com.example.winapp.feedback.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.winapp.feedback.BuildConfig;
import com.example.winapp.feedback.R;
import com.example.winapp.feedback.utils.Constants;
import com.example.winapp.feedback.utils.SharePref;
import com.example.winapp.feedback.utils.SystemPref;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HomeFragment extends Fragment implements View.OnClickListener {

    private static final int REQUEST_CHECK_SETTINGS = 100;
    private DatePicker start_date,end_date;
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;
    private CardView disease, note_book_layout, maintain_layout, sampling_layout;
    private TextView time, place_name, wind, humidity, temprature, rain_volume,uv_index;
    ImageView weather_image, smart_web, profile;
    private String TAG = HomeFragment.class.getSimpleName();
    SystemPref pref;
    SharePref sharePref;
    ProgressBar progress;

    public HomeFragment() {
        // Required empty public constructor
    }


    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setXML(view);
        init();
        startLocation();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showLastKnownLocation();
            }
        }, 2000);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * Setup Objects
     *
     * @param view
     */
    public void setXML(View view) {
        pref = new SystemPref();
        sharePref = new SharePref(getContext());
        disease = view.findViewById(R.id.disease);
        time = view.findViewById(R.id.time);
        place_name = view.findViewById(R.id.place_name);
        wind = view.findViewById(R.id.wind);
        humidity = view.findViewById(R.id.humidity);
        progress = view.findViewById(R.id.progress);
        temprature = view.findViewById(R.id.temprature);
        weather_image = view.findViewById(R.id.weather_image);
        note_book_layout = view.findViewById(R.id.note_book_layout);
        maintain_layout = view.findViewById(R.id.maintain_layout);
        sampling_layout = view.findViewById(R.id.sampling_layout);
        smart_web = view.findViewById(R.id.smart_web);
        rain_volume = view.findViewById(R.id.rain_volume);
        uv_index = view.findViewById(R.id.uv_index);
        disease.setOnClickListener(this);
        note_book_layout.setOnClickListener(this);
        maintain_layout.setOnClickListener(this);
        sampling_layout.setOnClickListener(this);
        smart_web.setOnClickListener(this);
        DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        time.setText(pref.getWeekDay() + " " + dateFormat.format(new Date()).toString());
    }

    public void init() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mSettingsClient = LocationServices.getSettingsClient(getActivity());
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                mCurrentLocation = locationResult.getLastLocation();

            }
        };
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(10000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    private void openSettings() {
        Intent intent = new Intent();
        intent.setAction(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package",
                BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    public void startLocation() {
        Dexter.withActivity(getActivity())
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        // mRequestingLocationUpdates = true;
                        startLocationUpdates();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        if (response.isPermanentlyDenied()) {
                            // open device settings when the permission is
                            // denied permanently
                            openSettings();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(getActivity(), new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");

                        //  Toast.makeText(getContext(), "Started location updates!", Toast.LENGTH_SHORT).show();

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                showLastKnownLocation();

                            }
                        }, 2000);
                        //updateLocationUI();
                    }
                })
                .addOnFailureListener(getActivity(), new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);

                                Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
                        }
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                showLastKnownLocation();

                            }
                        }, 2000);
                    }
                });
    }

    public void showLastKnownLocation() {

        if (mCurrentLocation != null) {
           /* Toast.makeText(Constants.getActivity(), "Lat: " + mCurrentLocation.getLatitude()
                    + ", Lng: " + mCurrentLocation.getLongitude(), Toast.LENGTH_LONG).show();*/
            sharePref.saveLatitude(String.valueOf(mCurrentLocation.getLatitude()));
            sharePref.saveLongitude(String.valueOf(mCurrentLocation.getLongitude()));

            setWeather(pref.convertToString(mCurrentLocation.getLatitude()),
                    pref.convertToString(mCurrentLocation.getLongitude()));
            setUVIndex(pref.convertToString(mCurrentLocation.getLatitude()),
                    pref.convertToString(mCurrentLocation.getLongitude()));
            progress.setVisibility(View.GONE);

        } else {
           // Toast.makeText(Constants.getActivity(), "Last known location is not available!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        // Resuming location updates depending on button state and
        // allowed permissions
        if (checkPermissions()) {
            startLocationUpdates();
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showLastKnownLocation();

            }
        }, 2000);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.disease:
                pref.openFragment(new DiseaseFragment(), getActivity());
                break;
            case R.id.maintain_layout:
                pref.openFragment(new MaintanesFragment(), getActivity());
                break;
            case R.id.note_book_layout:
                pref.openFragment(new FieldNoteBookFragment(), getActivity());
                break;
            case R.id.sampling_layout:
                pref.openFragment(new SamplingFragment(), getActivity());
                break;

        }
    }

    public void setWeather(String lat, String longi) {
        StringRequest request = new StringRequest(Request.Method.GET,
                Constants.LoadUrl(Constants.W_BASE_URL,lat, longi), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v(TAG + "weather=>", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has("name")) {
                        place_name.setText(jsonObject.getString("name"));
                    }
                    if (jsonObject.has("wind")) {
                        JSONObject windObj = jsonObject.getJSONObject("wind");
                        if (windObj.has("speed")) {
                            wind.setText("Viento: " + windObj.getString("speed"));
                        }
                    }
                    if (jsonObject.has("main")) {
                        JSONObject main = jsonObject.getJSONObject("main");
                        if (main.has("humidity")) {
                            humidity.setText("Humedad: " + main.getString("humidity") + "%");
                        }
                        if (main.has("temp")) {
                            temprature.setText(main.getString("temp"));
                        }
                    }
                    if (jsonObject.has("weather")) {
                        JSONArray weather = jsonObject.getJSONArray("weather");
                        JSONObject weatherOject = weather.getJSONObject(0);
                        if (weatherOject.has("icon")) {
                            Picasso.get().load(Constants.LoadImageUrl(weatherOject.getString("icon"))).into(weather_image);
                        }
                    }
                    if (jsonObject.has("rain")) {
                        JSONObject rain = jsonObject.getJSONObject("rain");
                        if (rain.has("h3")) {
                            rain_volume.setVisibility(View.VISIBLE);
                            rain_volume.setText("Precipitación: "+rain.getString("h3"));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        Volley.newRequestQueue(Constants.getActivity()).add(request);
    }
    public void setUVIndex(String lat, String longi) {
        StringRequest request = new StringRequest(Request.Method.GET,
                Constants.LoadUrl(Constants.WEATHER_UVINDEX,lat, longi), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.v(TAG + "weather=>", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has("value")) {
                        uv_index.setText("Índice UV: "+jsonObject.getString("value"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        Volley.newRequestQueue(Constants.getActivity()).add(request);
    }
}
