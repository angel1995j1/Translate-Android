package com.example.winapp.feedback.fragment;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.winapp.feedback.R;
import com.example.winapp.feedback.adapters.LocationAdapter;
import com.example.winapp.feedback.models.LocationModel;
import com.example.winapp.feedback.utils.RecyleViewHandler;
import com.example.winapp.feedback.utils.SharePref;
import com.example.winapp.feedback.utils.VerticalSpaceItemDecoration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LocationFragment extends Fragment {

    RecyclerView location_list;
    List<LocationModel> locationModelList;
    List<LocationModel> filterLocationModelList;
    EditText search;
    SharePref pref;
    LocationAdapter locationAdapter;
    public LocationFragment() {
        // Required empty public constructor
    }


    public static LocationFragment newInstance(String param1, String param2) {
        LocationFragment fragment = new LocationFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        locationModelList = new ArrayList<>();
        filterLocationModelList = new ArrayList<>();
        setXML(view);
    }

    public void setXML(View view){
        location_list  = view.findViewById(R.id.location_list);
        search  = view.findViewById(R.id.search);
        pref = new SharePref(getContext());
        setupRecyelView();
        setLocationList();


    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_location, container, false);
    }

    public void setupRecyelView(){
        RecyleViewHandler recyclerViewHandler = new RecyleViewHandler();
        location_list = recyclerViewHandler.
                settingRecyleView(location_list, LinearLayoutManager.VERTICAL,getContext());
        location_list.addItemDecoration(new VerticalSpaceItemDecoration(20));
        location_list.setItemAnimator(new DefaultItemAnimator());

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
    public void setLocationList(){
        LocationModel one = new LocationModel();
        one.setId(1);
        one.setEmail("administracion@agricolacastelao.com");
        one.setLocation("43.241832, -8.278580");
        one.setPhone("981/673547");
        one.setProvince("A CORUÑA");
        one.setCity("15318 - ABEGONDO");
        one.setAddress("SAN TIRSO DE MABEGONDO");
        one.setName("AGRICOLA CASTELAO, S.L.");
        // ================ TWO ===================
        LocationModel two = new LocationModel();
        two.setId(2);
        two.setLocation("38.241579, -0.686709");
        two.setEmail("agroelx@hotmail.com");
        two.setPhone("966/610600");
        two.setProvince("ALICANTE");
        two.setCity("03207 - ELCHE");
        two.setAddress("POLIG. ALCUBIA. PARTIDA ALZABARES BAJO\n" +
                "NAVE P-1, Nº 167");
        two.setName("AGROELX MAQUINARIA, S.L.");
        // ================ THREE ===================
        LocationModel three = new LocationModel();
        three.setId(3);
        three.setLocation("36.774550, -2.805564");
        three.setEmail("jesus@suministrosmartinez.es");
        three.setPhone("950/606666");
        three.setProvince("ALMERIA");
        three.setCity("04710 - SANTA MARIA DEL AGUILAR (EL EJIDO)");
        three.setAddress("AVDA. EL TREINTA, 251");
        three.setName("SUMINISTROS INDUSTRIALES MARTINEZ, S.L.");

        // ================ FOUR ===================
        LocationModel four = new LocationModel();
        four.setId(4);
        four.setLocation("43.393938, -5.788729");
        four.setEmail("administracion@casadomaquinaria.com");
        four.setPhone("985792920");
        four.setProvince("ASTURIAS");
        four.setCity("33429 - SIERO");
        four.setAddress("CARRETERA DE LA ESTACION-VIELLA Nº 18");
        four.setName("CASADO MAQUINARIA AGRICOLA S.L");

        // ================ five ===================
        LocationModel five = new LocationModel();
        five.setId(5);
        five.setLocation("38.927456, -6.379612");
        five.setEmail("daniel@extremenadecamiones.es");
        five.setPhone("924/371718");
        five.setProvince("BADAJOZ");
        five.setCity("06800 - MERIDA");
        five.setAddress("POL. IND. EL PRADO C/ ZARAGOZA Nº11");
        five.setName("EXTREMEÑA DE CAMIONES, S.A");

// ================ six ===================
        LocationModel six = new LocationModel();
        six.setId(6);
        six.setLocation("42.040897, -3.752202");
        six.setEmail("tallerclemen@tallerclemen.com");
        six.setPhone("947/172162");
        six.setProvince("BURGOS");
        six.setCity("09340 - LERMA");
        six.setAddress("CTRA. MADRID-IRUN, KM. 203");
        six.setName("CLEMENTINO GARCIA LOPEZ, S.L.");
        // ================ SEVEN ===================
        LocationModel seven = new LocationModel();
        seven.setId(7);
        seven.setLocation("41.660619, -4.764975");
        seven.setEmail("monica@miguelgutierrezsl.es");
        seven.setPhone("983/350951");
        seven.setProvince("VALLADOLID");
        seven.setCity("47610 - ZARATAN");
        seven.setAddress("CTRA. DE LEON, KM. 195");
        seven.setName("MIGUEL GUTIERREZ DEL RIO, S.L");

        // ================ eight ===================
        LocationModel eight = new LocationModel();
        eight.setId(7);
        eight.setLocation("41.807742, -5.144674");
        eight.setEmail("administracion@argasa.es");
        eight.setPhone("983 714 481");
        eight.setProvince("VALLADOLID");
        eight.setCity("47830 - TORDEHUMOS");
        eight.setAddress("CTRA. MEDINA DE RIOSECO - TORO, KM.12");
        eight.setName("TALLERES ARGASA, S.L");

        locationModelList.add(one);
        locationModelList.add(two);
        locationModelList.add(three);
        locationModelList.add(four);
        locationModelList.add(five);
        locationModelList.add(six);
        locationModelList.add(seven);
        locationModelList.add(eight);
        Location startPoint=new Location("locationA");
        startPoint.setLatitude( pref.getLatitude());
        startPoint.setLongitude(pref.getLongitude());
        for (int i = 0; i <locationModelList.size() ; i++) {
            String[] token = locationModelList.get(i).getLocation().split(",");
            Location endPoint=new Location("locationB");
            endPoint.setLatitude(Double.parseDouble(token[0]));
            endPoint.setLongitude(Double.parseDouble(token[1]));
            int distane = (int)startPoint.distanceTo(endPoint);
            locationModelList.get(i).setDistance(distane);
        }
        Collections.sort(locationModelList);
        locationAdapter = new LocationAdapter(locationModelList,getContext());
        filterLocationModelList.addAll(locationModelList);
        location_list.setAdapter(locationAdapter);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filterList(s.toString(),locationModelList);
                if(s.toString().equalsIgnoreCase("")){

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
    public void filterList(String data,  List<LocationModel> locationModelList1) {
        if (data.isEmpty()) {
            locationModelList.clear();
            locationModelList.addAll(filterLocationModelList);
        } else {
            List<LocationModel>  filterData = new ArrayList<>();
            for (LocationModel locationModel : locationModelList1) {
                if (locationModel.getProvince().toLowerCase().contains(data.toLowerCase())) {
                    filterData.add(locationModel);
                }
            }
            if(filterData.size()>0) {
                locationModelList.clear();
                locationModelList.addAll(filterData);
            }else{
                locationModelList.clear();
                locationModelList.addAll(filterLocationModelList);
            }
            //   locationModelList =  filterData ;
        }
        locationAdapter.notifyDataSetChanged();

    }
}
