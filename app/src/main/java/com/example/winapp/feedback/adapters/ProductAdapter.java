package com.example.winapp.feedback.adapters;

import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.lifecycle.Observer;
import com.example.winapp.feedback.R;
import com.example.winapp.feedback.livedata_Model.EmptyModel;
import com.example.winapp.feedback.livedata_Model.ProductClickEvent;
import com.example.winapp.feedback.models.Products;
import com.example.winapp.feedback.utils.Constants;
import com.example.winapp.feedback.utils.SystemPref;

import java.util.List;

public class ProductAdapter  extends RecyclerView.Adapter<ProductAdapter.ProductHolder>  {
    List<Products> productsList;
    Context context;
    ProductClickEvent productClickEvent;
    EmptyModel emptyModel;

    int type;
    public ProductAdapter(List<Products> productsList, Context context,int type , ProductClickEvent productClickEvent ) {
        this.productsList = productsList;
        this.context = context;
        this.productClickEvent = productClickEvent;
        this.type = type;
        emptyModel = ViewModelProviders.of((FragmentActivity) context).get(EmptyModel.class);

    }

    @NonNull
    @Override
    public ProductHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = null;
        if(type==Constants.PRODUCT_LIST) {
            view = SystemPref.getLayout(viewGroup,R.layout.product_list);
        }
        else if(type==Constants.CART_LIST){
            view = SystemPref.getLayout(viewGroup,R.layout.card_lists);
        }
        return new ProductHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductHolder holder, int i) {
        Products products = productsList.get(i);
        holder.productImage.setImageResource(products.getProductImage());
        holder.productName.setText(products.getProductName());
        holder.productNo.setText(String.valueOf(products.getProductId()));
        if(type==Constants.CART_LIST){
            holder.remove_list.setOnClickListener(v2->{
                productClickEvent.deleteItem(i);
                emptyModel.removeValue().setValue(products);
            });
        }
        holder.add_tocard.setOnClickListener(v->{
            productClickEvent.addToCard(products);

            int count = Integer.parseInt(holder.quantity.getText().toString());
            holder.quantity.setText(String.valueOf(count+1));

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        holder.remove_card.setOnClickListener(v1->{
            productClickEvent.removeCard(products);
            int count = Integer.parseInt(holder.quantity.getText().toString());
            if(count>0) {
                holder.quantity.setText(String.valueOf(count - 1));
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        holder.quantity.setText(String.valueOf(products.getCardQuantity()));
        Observer<Products> productsObserver = products1 -> {
            if(products1.getProductId()== products.getProductId()){
                if(type==Constants.PRODUCT_LIST) {
                    holder.quantity.setText("0");
                }
            }
        };
        emptyModel.removeValue().observe((FragmentActivity) context,productsObserver);
    }

    @Override
    public int getItemCount() {
        return productsList.size();
    }


    protected class ProductHolder extends RecyclerView.ViewHolder {
        TextView productName,productNo,quantity;
        ImageView productImage,add_tocard,remove_card,remove_list;
        public ProductHolder(@NonNull View view) {
            super(view);
            productName = view.findViewById(R.id.product_name);
            productNo = view.findViewById(R.id.product_no);
            productImage = view.findViewById(R.id.product_image);
            add_tocard = view.findViewById(R.id.add_tocard);
            quantity = view.findViewById(R.id.quantity);
            remove_card = view.findViewById(R.id.remove_card);
            remove_list = view.findViewById(R.id.remove_list);
        }
    }

}