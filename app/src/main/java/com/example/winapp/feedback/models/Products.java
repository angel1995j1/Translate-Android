package com.example.winapp.feedback.models;

public class Products {
    private int productId;
    private int catId;
    private String productName;
    private int productImage;
    private int cardQuantity;

    public Products(int productId, int catId, String productName, int productImage,int cardQuantity) {
        this.productId = productId;
        this.catId = catId;
        this.productName = productName;
        this.productImage = productImage;
        this.cardQuantity = cardQuantity;
    }

    public int getCardQuantity() {
        return cardQuantity;
    }

    public void setCardQuantity(int cardQuantity) {
        this.cardQuantity = cardQuantity;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductImage() {
        return productImage;
    }

    public void setProductImage(int productImage) {
        this.productImage = productImage;
    }
}
