package com.example.winapp.feedback.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.winapp.feedback.R;
import com.example.winapp.feedback.models.UserModel;
import com.example.winapp.feedback.utils.Constants;
import com.example.winapp.feedback.utils.EditTextHelper;
import com.example.winapp.feedback.utils.SharePref;

public class ProfileFragment extends Fragment {

    EditText firstName,lastName,address,telephone;
    Button submit;
    EditTextHelper editTextHelper;
    SharePref pref;
    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pref =  new SharePref(getContext());
        UserModel userModel = pref.getObject();
        editTextHelper = new EditTextHelper();
        setXML(view);
        setData(userModel);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void setXML(View view){
        SharePref pref =  new SharePref(getContext());
        firstName = view.findViewById(R.id.first_name);
        lastName = view.findViewById(R.id.last_name);
        address = view.findViewById(R.id.address);
        telephone = view.findViewById(R.id.telephone);
        submit = view.findViewById(R.id.submit);
        submit.setOnClickListener(v->{
            if(checkfields()){
                UserModel userModel = new UserModel();
                userModel.setId(1);
                userModel.setFirstName(firstName.getText().toString());
                userModel.setLastName(lastName.getText().toString());
                userModel.setAddress(address.getText().toString());
                userModel.setTelephone(telephone.getText().toString());
                pref.saveUserObject(userModel);
                pref.userStatus(true);
                Toast.makeText(getContext(),
                        getContext().getString(R.string.profile_update),
                        Toast.LENGTH_LONG).show();
                EditTextHelper.hideSoftKeyboard(Constants.getActivity());
            }
        });

    }
    public boolean checkfields(){

        if(editTextHelper.isEmptyField(firstName)){
            firstName.setError(getString(R.string.required));
            return  false;
        }
        else if(editTextHelper.isEmptyField(lastName)){
            lastName.setError(getString(R.string.required));
            return false;
        }
        else if(editTextHelper.isEmptyField(telephone)){
            telephone.setError(getString(R.string.required));
            return false;
        }
        else if(editTextHelper.isEmptyField(address)){
            address.setError(getString(R.string.required));
            return false;
        }
        return true;
    }
    public void setData(UserModel userModel){
        firstName.setText(userModel.getFirstName());
        lastName.setText(userModel.getLastName());
        telephone.setText(userModel.getTelephone());
        address.setText(userModel.getAddress());
    }
}
