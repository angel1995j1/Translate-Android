package com.example.winapp.feedback.fragment;

import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.winapp.feedback.R;
import com.example.winapp.feedback.adapters.ProductAdapter;
import com.example.winapp.feedback.livedata_Model.DataModel;
import com.example.winapp.feedback.livedata_Model.ProductClickEvent;
import com.example.winapp.feedback.livedata_Model.RemoveCardModel;
import com.example.winapp.feedback.models.Products;
import com.example.winapp.feedback.utils.Constants;

import java.util.ArrayList;
import java.util.List;


public class ProductsFragment extends Fragment implements ProductClickEvent {

    ProductAdapter adapter;
    static String TYPE="type";
    RecyclerView product_list;
    ProgressBar progressBar;
    private List<Products> productList,productList1;
    private int typ;
    private DataModel dataModel;
    private RemoveCardModel removeCardModel;

    public ProductsFragment() {
        // Required empty public constructor
    }


    public static ProductsFragment newInstance(int type) {
        ProductsFragment fragment = new ProductsFragment();
        Bundle args = new Bundle();
        args.putInt(TYPE,type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_products, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        typ = bundle.getInt(TYPE);
      //  Toast.makeText(getContext(),String.valueOf(typ),Toast.LENGTH_LONG).show();
        setXML(view);
    }
    public void setXML(View view){
        product_list = view.findViewById(R.id.product_list);
        progressBar = view.findViewById(R.id.progress_circular);
        setList();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
    public void setList(){
        productList = new ArrayList<>();
        productList1 = new ArrayList<>();
        productList.add(new Products(34289,1,
                "Aerosolo 500ml anti pinchazo",R.drawable.prod_2,0));
        productList.add(new Products(1378,1,
                "Optico H3 Oval",R.drawable.prod_22,0));

        productList.add(new Products(2145,1,
                "Cierre goma Technik plus oko",R.drawable.prod_3,0));

        productList.add(new Products(2342,1,
                "Anillo Reducción",R.drawable.prod_4,0));

        productList1.add(new Products(424234,1,
                "Rex3F",R.drawable.prod_2,0));
        productList1.add(new Products(25242,1,
                "ISM-ARM",R.drawable.prod_22,0));

        product_list.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        if(typ==1){
            adapter = new ProductAdapter(productList,getContext(), Constants.PRODUCT_LIST,this);
        }
        else{
            adapter = new ProductAdapter(productList1,getContext(),Constants.PRODUCT_LIST,this);
        }
        product_list.setAdapter(adapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        dataModel = ViewModelProviders.of(getActivity()).get(DataModel.class);
        removeCardModel = ViewModelProviders.of(getActivity()).get(RemoveCardModel.class);
    }

    @Override
    public void addToCard(Products products) {
        dataModel.getCurrentName().setValue(products);
    }

    @Override
    public void removeCard(Products products) {
        removeCardModel.getProductRemove().setValue(products);
    }

    @Override
    public void deleteItem(int postion) {

    }
}
