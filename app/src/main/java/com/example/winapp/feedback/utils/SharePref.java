package com.example.winapp.feedback.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.winapp.feedback.models.UserModel;
import com.google.gson.Gson;

public class SharePref {
    private static final String USER_OBJECT = "user_object";
    private static final String PREF_NAME = "landini";
    private static final String IS_UER = "is_user";
    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";
    private SharedPreferences preferences;
    private Context context;

    public SharePref( Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE);
    }
    public void saveUserObject(UserModel userModel){
        String user  = new Gson().toJson(userModel);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(USER_OBJECT,user);
        editor.commit();
    }
    public UserModel getObject(){
        String user= preferences.getString(USER_OBJECT,null);
        return new Gson().fromJson(user,UserModel.class);
    }
    public void userStatus(boolean status){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(IS_UER,status);
        editor.commit();
    }
    public boolean isUser(){
        return preferences.getBoolean(IS_UER,false);
    }
    public void saveLatitude(String latitude){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(LATITUDE,latitude);
        editor.commit();
    }
    public void saveLongitude(String latitude){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(LONGITUDE,latitude);
        editor.commit();
    }
    public double getLatitude(){
        return Double.parseDouble(preferences.getString(LATITUDE,"0"));
    }
    public double getLongitude(){
        return Double.parseDouble(preferences.getString(LONGITUDE,"0"));
    }
}
