package com.example.winapp.feedback;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.winapp.feedback.fragment.HomeFragment;
import com.example.winapp.feedback.fragment.ReadTasksFragments;
import com.example.winapp.feedback.utils.Constants;
import com.example.winapp.feedback.utils.SystemPref;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private String TAG = MainActivity.class.getSimpleName();
    SystemPref pref;
    RelativeLayout card_data;
    TextView quantity;
    ImageView notification;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        quantity = findViewById(R.id.quantity);
        notification = findViewById(R.id.notification);
        pref = new SystemPref();
        Constants.setActivity(this);
        pref.openFragmentNoStack(new HomeFragment(),this);
        notification.setOnClickListener(view->{
            if(getSupportFragmentManager().findFragmentById(R.id.frame) instanceof ReadTasksFragments){

            }else{
                pref.openFragment(new ReadTasksFragments(),this);
            }
        });
    }
    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
        }
    }

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount()>0){
            getSupportFragmentManager().popBackStack();
        }else{
            super.onBackPressed();
        }
    }




}

