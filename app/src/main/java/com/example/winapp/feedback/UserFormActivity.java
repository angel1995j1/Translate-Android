package com.example.winapp.feedback;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.example.winapp.feedback.models.UserModel;
import com.example.winapp.feedback.utils.EditTextHelper;
import com.example.winapp.feedback.utils.SharePref;

public class UserFormActivity extends AppCompatActivity {

    EditText firstName,lastName,address,telephone;
    Button submit;
    EditTextHelper editTextHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_form);
        editTextHelper = new EditTextHelper();
        setXML();
       // EditTextHelper.hideSoftKeyboard(UserFormActivity.this);

    }
    public void setXML(){
        SharePref pref =  new SharePref(this);
        firstName = findViewById(R.id.first_name);
        lastName = findViewById(R.id.last_name);
        address = findViewById(R.id.address);
        telephone = findViewById(R.id.telephone);
        submit = findViewById(R.id.submit);
        submit.setOnClickListener(v->{
            if(checkfields()){
                UserModel userModel = new UserModel();
                userModel.setId(1);
                userModel.setFirstName(firstName.getText().toString());
                userModel.setLastName(lastName.getText().toString());
                userModel.setAddress(address.getText().toString());
                userModel.setTelephone(telephone.getText().toString());
                pref.saveUserObject(userModel);
                pref.userStatus(true);
                clearFields();
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
                finish();
            }
        });

    }
    public boolean checkfields(){

        if(editTextHelper.isEmptyField(firstName)){
            firstName.setError(getString(R.string.required));
            return  false;
        }
        else if(editTextHelper.isEmptyField(lastName)){
            lastName.setError(getString(R.string.required));
            return false;
        }
        else if(editTextHelper.isEmptyField(telephone)){
            telephone.setError(getString(R.string.required));
            return false;
        }
        else if(editTextHelper.isEmptyField(address)){
            address.setError(getString(R.string.required));
            return false;
        }
        return true;
    }
    public void clearFields(){
        firstName.setText("");
        lastName.setText("");
        address.setText("");
        telephone.setText("");
    }
}
