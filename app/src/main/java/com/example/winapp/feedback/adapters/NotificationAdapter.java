package com.example.winapp.feedback.adapters;

import android.app.Dialog;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import br.com.felix.imagezoom.ImageZoom;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alespero.expandablecardview.ExpandableCardView;
import com.example.winapp.feedback.R;
import com.example.winapp.feedback.models.NotificationsModel;
import com.example.winapp.feedback.utils.Constants;
import com.example.winapp.feedback.utils.SystemPref;
import com.github.chrisbanes.photoview.PhotoView;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationHolder> {
    List<NotificationsModel> notifications;
    Context context;

    public NotificationAdapter(List<NotificationsModel> notifications, Context context) {
        this.notifications = notifications;
        this.context = context;
    }

    @NonNull
    @Override
    public NotificationHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.notification_list,
                viewGroup,false);
        return new NotificationHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationHolder holder, int i) {
        NotificationsModel notificationsModel = notifications.get(i);
        holder.notification.setTitle(Html.fromHtml("<b>"+notificationsModel.getType()+": "+notificationsModel.getTitle()+"</b>").toString());
        holder.title.setText(notificationsModel.getTitle());
        holder.type.setText(notificationsModel.getType());
        holder.title1.setText(notificationsModel.getTitle());
        holder.type1.setText(notificationsModel.getType());
        holder.date.setText(notificationsModel.getDate());
        holder.author.setText(notificationsModel.getAuthor());
        holder.comment.setText(notificationsModel.getComments());
      //  holder.address.setText(notificationsModel.getAddress());
        holder.image.setImageResource(notificationsModel.getImage());
        holder.open_map.setOnClickListener(v->{
            String[] tokenizer =notificationsModel.getAddress().split(",");
            String latitude = tokenizer[0];
            String longitude = tokenizer[1];
            SystemPref.openMap(context,latitude,longitude);
        });
        holder.image.setOnClickListener(v1->{
            zoomImage(notificationsModel.getImage());
        });
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    protected class NotificationHolder extends RecyclerView.ViewHolder{
        TextView title,title1,type,type1,date,author,address,comment;
        ImageView image;
        ExpandableCardView notification;
        RelativeLayout open_map;
        public NotificationHolder(@NonNull View view) {
            super(view);
            title = view.findViewById(R.id.title);
            type = view.findViewById(R.id.type);
            type1 = view.findViewById(R.id.type1);
            title1 = view.findViewById(R.id.title1);
            date = view.findViewById(R.id.date);
            author = view.findViewById(R.id.author);
           // address = view.findViewById(R.id.address);
            image = view.findViewById(R.id.image);
            notification = view.findViewById(R.id.notification);
            open_map = view.findViewById(R.id.open_map);
            comment = view.findViewById(R.id.comments);
            image.setAdjustViewBounds(true);
        }
    }
    public void zoomImage(int imageSource){
        Dialog dialog = new Dialog(Constants.getActivity());
        dialog.setContentView(R.layout.zoom_image);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        PhotoView imageView = dialog.findViewById(R.id.zoom_image);
        imageView.setImageResource(imageSource);
        dialog.show();
    }
}
